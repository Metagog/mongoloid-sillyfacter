from __future__ import print_function
from __future__ import absolute_import
import os
import platform
import re
import logging
import inspect
import yaml
import shlex
from ..common import *
import sillyfacter.config


MODULEFILE = re.sub('\.py', '',
                    os.path.basename(inspect.stack()[0][1]))


def fetch(host=platform.node()):
    ldict = {"step": MODULEFILE + "/" + inspect.stack()[0][3],
             "hostname": platform.node().split(".")[0]}
    l = logging.LoggerAdapter(fetch_lg(), ldict)
    #
    l.info("Gathering host ENVOY info...")

    retval = {}
    retval["custom"] = {"envoy": {"envoy_present": False}}
    ret = retval["custom"]["envoy"]
    if not os.path.exists('/var/tmp/envoy.dat'):
        return retval

    command_line = "/usr/cisco/bin/perl -MYAML -le " + \
                   "'print YAML::Dump( do shift )' " + \
                   "/var/tmp/envoy.dat"
    envoy = Command(shlex.split(command_line))
    if envoy.run(maxtime=60) is not None:
        output = re.sub('\t', "  ", "".join(envoy.output))
        try:
            e = yaml.load(output)
            ret["text"] = {}
            for k in e.keys():
                if re.search("^(audits_|miscinfo_).*", k):
                    k_new = re.sub('[^_A-Z0-9a-z]', '_', k)
                    if type(e[k]) is list:
                        ret["text"][k_new] = "\n".join(e[k])
                elif k in ["firmware", "defaultroute",
                           "internal_ver", "maker", "consoleuser",
                           "machineid", "model", "mos",
                           "primaryint", "primarymacaddr", "profile",
                           "selfgenid", "serial", "supportgroup",
                           "systemid", "timezone", "user", "uptime"]:
                    ret[k] = e[k]
                elif k == "packages":
                    ret[k] = [re.sub('[^a-zA-Z0-9_-]', '', _.split()[0])
                              for _ in e[k]]
        except Exception as _:
            if sillyfacter.config.STRICT:
                raise
            if sillyfacter.config.DEBUG:
                l.exception("")
            l.error("Unable to generate custom output, got: {}".format(_))
            ret["envoy_present"] = False
        else:
            ret["envoy_present"] = True
    else:
        ret["envoy_present"] = False
    return retval


def output(f):
    ldict = {"step": MODULEFILE + "/" + inspect.stack()[0][3],
             "hostname": platform.node().split(".")[0]}
    l = logging.LoggerAdapter(fetch_lg(), ldict)
    #
    l.info("Generating ENVOY custom output")
    retval = {"envoy": {}}
    fetched = retval["envoy"]
    try:
        for item, val in f["custom"]["envoy"].iteritems():
            fetched[item] = val
    except Exception as _:
        if sillyfacter.config.STRICT:
            raise
        if sillyfacter.config.DEBUG:
            l.exception("")
        l.error("Unable to generate custom output, got: {}".format(_))
    return retval
